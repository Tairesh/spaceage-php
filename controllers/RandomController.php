<?php


namespace app\controllers;


use app\models\astro\generators\StarGenerator;
use app\models\astro\generators\SystemGenerator;
use yii\web\Response;

class RandomController extends base\AppController
{

    /**
     *
     * @return Response|string
     */
    public function actionSystem()
    {
        $generator = SystemGenerator::new()
                            ->withRandomName()
                            ->withRandomPosition()
                            ->withRandomStars();
        $system = $generator->generate();
        print '<pre>';
        /** @noinspection ForgottenDebugOutputInspection */
        var_dump($generator, $system, $system->stars);
    }

}