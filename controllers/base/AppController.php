<?php

namespace app\controllers\base;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\IdentityInterface;
use yii\web\Response;
use app\models\User;

/**
 * Description of AppController
 *
 * @author ilya
 * @property User $user
 */
class AppController extends Controller
{
    
//    public $layout = 'app';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * @return User|IdentityInterface
     */
    protected function getUser()
    {
        if (Yii::$app->user->isGuest) {
            throw new HttpException(401);
        }
        return Yii::$app->user->identity;
    }
    
    /**
     * 
     * @return array
     */
    protected function ok(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => 'ok'];
    }
    
    /**
     * 
     * @param mixed $e
     * @return array
     */
    protected function error($e): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => 'error', 'error' => $e];
    }
    
}
