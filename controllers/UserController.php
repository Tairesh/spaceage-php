<?php


namespace app\controllers;

use app\models\auth\AvatarForm;
use app\models\User;
use app\controllers\base\AppController;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends AppController
{

    public $defaultAction = 'view';

    /**
     * User profile
     * @param int $id user ID
     * @return Response|string
     */
    public function actionView(int $id = null)
    {
        if (!$id) {
            return $this->redirect(['/user/view', 'id' => $this->user->id]);
        }

        $isOwner = $this->user->id === $id;
        $form = new AvatarForm();
        if ($isOwner && Yii::$app->request->isPost && $form->load(Yii::$app->request->post())) {
            $form->proceed();
        }
        return $this->render('view', [
            'model' => $isOwner ? $this->user : $this->getModel($id),
            'viewer' => $this->user,
            'isOwner' => $isOwner,
            'avatarFormModel' => $form,
        ]);
    }

    public function actionAvatar()
    {
        $this->redirect(['/user']);
    }

    protected function getModel(int $id): User
    {
        $model = User::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('User not found');
        }
        return $model;
    }

}