<?php

namespace app\models\astro;

use app\models\base\ActiveRecord;

/**
 * This is the model class for table "systems".
 *
 * @property int $id
 * @property string $name
 * @property int $x
 * @property int $y
 *
 * @property Planet[] $planets
 * @property Moon[] $moons
 * @property Star[] $stars
 *
 * @property int $starsCount
 */
class System extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'systems';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'x', 'y'], 'required'],
            [['x', 'y'], 'default', 'value' => null],
            [['x', 'y'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['x', 'y'], 'unique', 'targetAttribute' => ['x', 'y']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'x' => 'X',
            'y' => 'Y',
        ];
    }

    /**
     * Gets query for [[Planets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlanets()
    {
        return $this->hasMany(Planet::class, ['systemId' => 'id']);
    }

    /**
     * Gets query for [[Moons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMoons()
    {
        return $this->hasMany(Moon::class, ['systemId' => 'id']);
    }

    /**
     * Gets query for [[Stars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStars()
    {
        return $this->hasMany(Star::class, ['systemId' => 'id']);
    }

    /**
     * Getter for $starsCount
     * @return int
     */
    public function getStarsCount(): int
    {
        return (int)$this->getStars()->count();
    }

}
