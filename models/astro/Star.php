<?php

namespace app\models\astro;

use app\models\base\ActiveRecord;

/**
 * This is the model class for table "stars".
 *
 * @property int $id
 * @property int $systemId
 * @property string $name
 * @property float $mass
 * @property float $radius
 * @property float $luminosity
 * @property float $temperature
 * @property int $type
 * @property int|null $spectre
 * @property int|null $spectreNumber
 * @property int|null $orbitAroundId
 * @property float|null $orbitRadius In megametres
 * @property float|null $orbitEccentricity
 *
 * @property Planet[] $planets
 * @property Star $orbitAround
 * @property Star[] $stars
 * @property System $system
 */
class Star extends ActiveRecord
{

    public const TYPE_BROWN_DWARF = 1;
    public const TYPE_DWARF = 2;
    public const TYPE_WHITE_DWARF = 3;
    public const TYPE_SUBDWARF = 4;
    public const TYPE_SUBGIANT = 5;
    public const TYPE_GIANT = 6;
    public const TYPE_BRIGHT_GIANT = 7;
    public const TYPE_SUPERGIANT = 8;
    public const TYPE_HYPERGIANT = 9;
    public const TYPE_NEUTRON_STAR = 10;
    public const TYPE_BLACK_HOLE = 11;

    public const SPECTRE_O = 1;
    public const SPECTRE_B = 2;
    public const SPECTRE_A = 3;
    public const SPECTRE_F = 4;
    public const SPECTRE_G = 5;
    public const SPECTRE_K = 6;
    public const SPECTRE_M = 7;
    public const SPECTRE_L = 8;
    public const SPECTRE_T = 9;
    public const SPECTRE_Y = 10;
    public const SPECTRE_DA = 11;
    public const SPECTRE_DB = 12;
    public const SPECTRE_DQ = 13;
    public const SPECTRE_DO = 14;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['systemId', 'name', 'mass', 'radius', 'luminosity', 'temperature', 'type'], 'required'],
            [['systemId', 'type', 'spectre', 'spectreNumber', 'orbitAroundId'], 'default', 'value' => null],
            [['systemId', 'type', 'spectre', 'spectreNumber', 'orbitAroundId'], 'integer'],
            [['mass', 'radius', 'luminosity', 'temperature', 'orbitRadius', 'orbitEccentricity'], 'number'],
            [['name'], 'string', 'max' => 128],
            [['orbitAroundId'], 'exist', 'skipOnError' => true, 'targetClass' => __CLASS__, 'targetAttribute' => ['orbitAroundId' => 'id']],
            [['systemId'], 'exist', 'skipOnError' => true, 'targetClass' => System::class, 'targetAttribute' => ['systemId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'systemId' => 'System ID',
            'name' => 'Name',
            'mass' => 'Mass',
            'radius' => 'Radius',
            'luminosity' => 'Luminosity',
            'temperature' => 'Temperature',
            'type' => 'Type',
            'spectre' => 'Spectre',
            'spectreNumber' => 'Spectre Number',
            'orbitAroundId' => 'Orbit Around ID',
            'orbitRadius' => 'Orbit Radius',
            'orbitEccentricity' => 'Orbit Eccentricity',
        ];
    }

    /**
     * Gets query for [[Planets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlanets()
    {
        return $this->hasMany(Planet::class, ['orbitAroundId' => 'id']);
    }

    /**
     * Gets query for [[OrbitAround]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrbitAround()
    {
        return $this->hasOne(__CLASS__, ['id' => 'orbitAroundId']);
    }

    /**
     * Gets query for [[Stars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStars()
    {
        return $this->hasMany(__CLASS__, ['orbitAroundId' => 'id']);
    }

    /**
     * Gets query for [[System]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSystem()
    {
        return $this->hasOne(System::class, ['id' => 'systemId']);
    }
}
