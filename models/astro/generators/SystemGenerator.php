<?php


namespace app\models\astro\generators;

use app\helpers\ArrayHelper;
use app\models\astro\Star;
use app\models\astro\System;
use yii\base\Model;

class SystemGenerator extends Model
{

    public string $name;
    public int $x;
    public int $y;

    public int $starsCount;

    /**
     * @var StarGenerator[]
     */
    public array $stars = [];

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['name', 'x', 'y'], 'required'],
            [['name', 'x', 'y'], 'safe'],
        ];
    }

    public const NAME_PARTS = [
        'en', 'la', 'can', 'be',
        'and', 'phi', 'eth', 'ol',
        've', 'ho', 'a', 'lia',
        'an', 'ar', 'ur', 'mi',
        'in', 'ti', 'qu', 'so',
        'ed', 'ess', 'ex', 'io',
        'ce', 'ze', 'fa', 'ay',
        'wa', 'da', 'ack', 'gre',
    ];

    public const MULTISTAR_CHANCE = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                     2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4, 5];

    public static function randomName(): string
    {
        $parts = random_int(2, 4);
        $name = '';
        for ($i = 0; $i < $parts; $i++) {
            $name .= ArrayHelper::randomValue(self::NAME_PARTS);
        }
        return ucfirst($name);
    }

    public static function randomStarsCount(): int
    {
        return ArrayHelper::randomValue(self::MULTISTAR_CHANCE);
    }

    public static function new(): self
    {
        $generator = new self();
        $generator->starsCount = self::randomStarsCount();
        return $generator;
    }

    public function withPosition(int $x, int $y): self
    {
        $this->x = $x;
        $this->y = $y;
        return $this;
    }

    public function withRandomPosition(): self
    {
        $this->x = random_int(-10000, 10000);
        $this->y = random_int(-10000, 10000);
        return $this;
    }

    public function withName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function withRandomName(): self
    {
        $this->name = self::randomName();
        return $this;
    }

    public function withRandomStars(): self
    {
        for ($i = 0; $i < $this->starsCount; $i++) {
            $starGenerator = StarGenerator::new($i > 0 ? $this->stars[0]->type: null)->withRandomSpectre();
            $this->stars[$i] = $starGenerator;
        }
        uasort($this->stars, fn(StarGenerator $o1, StarGenerator $o2) => $o2->mass <=> $o1->mass);
        foreach ($this->stars as $i => &$star) {
            $star->name = $this->name . ($this->starsCount > 1 ? ' ' . chr($i+65) : '');
        }
        unset($star);
        if ($this->starsCount > 1) {
            for ($i = 1; $i < $this->starsCount; $i++) {
                $this->stars[$i]->orbitAroundN = random_int(0, $i-1);
                $this->stars[$i]->orbitEccentricity = random_int(0, 1000) / 1000;

                $maxOrbit = 1000000;
                $minOrbit = 10;
                if ($this->stars[$this->stars[$i]->orbitAroundN]->orbitRadius) {
                    if (mt_rand()/mt_getrandmax() > 0.5) {
                        $maxOrbit = (int)($this->stars[$this->stars[$i]->orbitAroundN]->orbitRadius * 0.8);
                    } else {
                        $minOrbit = (int)($this->stars[$this->stars[$i]->orbitAroundN]->orbitRadius * 2.0);
                    }
                }
                $this->stars[$i]->orbitRadius = abs(sqrt(-2*log(mt_rand()/mt_getrandmax()))*cos(2* M_PI *(mt_rand()/mt_getrandmax()))*$maxOrbit/10) + $minOrbit;
            }
        }
        return $this;
    }

    public function generate(): System
    {
        $system = new System(['x' => $this->x, 'y' => $this->y, 'name' => $this->name]);
        $system->save();
        $stars = [];
        ksort($this->stars);
        foreach ($this->stars as $i => $starGenerator) {
            if ($i > 0 && $starGenerator->orbitAroundN !== null) {
                $starGenerator->orbitAroundId = $stars[$starGenerator->orbitAroundN]->id;
            }
            $star = $starGenerator->generate($system);
            if (!$star->save()) {
                print_r($star->getErrors());
            }
            $stars[$i] = $star;
        }
        $system->refresh();
        return $system;
    }

}