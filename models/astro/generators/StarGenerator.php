<?php


namespace app\models\astro\generators;

use app\models\astro\Star;
use app\models\astro\System;
use yii\base\Model;
use app\helpers\ArrayHelper;

class StarGenerator extends Model
{

    public string $name;
    public float $mass;
    public float $radius;
    public float $luminosity;
    public float $temperature;
    public int $type;
    public ?int $spectre = null;
    public ?int $spectreNumber = null;
    public ?int $orbitAroundN = null;
    public ?float $orbitRadius = null;
    public ?float $orbitEccentricity = null;

    public ?int $orbitAroundId = null;

    public const WHITE_DWARF_SPECTRES = [
        Star::SPECTRE_DA, Star::SPECTRE_DB,
        Star::SPECTRE_DQ, Star::SPECTRE_DO
    ];

    public const BROWN_DWARFS_SPECTRES = [
        Star::SPECTRE_M, Star::SPECTRE_L, Star::SPECTRE_T, Star::SPECTRE_Y
    ];

    public const SOL_TEMPERATURE = 5780;

    public const STAR_PARAMS_BY_SPECTRE = [
        //                       min and max temp, mass and radius (in that exactly order)
        Star::TYPE_NEUTRON_STAR => [
            null => [100000, 1000000, 1.1, 3.0, 0.00001, 0.00002],
        ],
        Star::TYPE_BLACK_HOLE => [
            null => [null, null, 5.0, 3.0, 0, 0],
        ],
        Star::TYPE_BROWN_DWARF => [
            Star::SPECTRE_M => [2000, 2500, 0.002, 0.08, 0.08, 0.1],
            Star::SPECTRE_L => [1500, 2000, 0.002, 0.06, 0.07, 0.09],
            Star::SPECTRE_T => [700, 1500, 0.002, 0.05, 0.07, 0.09],
            Star::SPECTRE_Y => [300, 700, 0.001, 0.04, 0.07, 0.09],
        ],
        Star::TYPE_WHITE_DWARF => [
            Star::SPECTRE_DA => [4000, 20000, 0.2, 1.4, 0.008, 0.02],
            Star::SPECTRE_DB => [12000, 30000, 0.2, 1.4, 0.008, 0.02], // @TODO: audit mass and radius of not A-class white dwarfs
            Star::SPECTRE_DQ => [45000, 100000, 0.2, 1.4, 0.008, 0.02],
            Star::SPECTRE_DO => [12000, 30000, 0.2, 1.4, 0.008, 0.02],
        ],
        Star::TYPE_DWARF => [
            Star::SPECTRE_O => [30000, 50000, 16, 90, 6.6, 10],
            Star::SPECTRE_B => [10000, 30000, 2.1, 16, 1.8, 6.6],
            Star::SPECTRE_A => [7500, 10000, 1.4, 2.1, 1.4, 1.8],
            Star::SPECTRE_F => [6000, 7500, 1.04, 1.4, 1.15, 1.4],
            Star::SPECTRE_G => [5200, 6000, 0.8, 1.04, 0.96, 1.15],
            Star::SPECTRE_K => [3700, 5200, 0.45, 0.8, 0.7, 0.96],
            Star::SPECTRE_M => [2300, 3700, 0.08, 0.45, 0.08, 0.7],
        ],
        Star::TYPE_SUBDWARF => [
            Star::SPECTRE_O => [30000, 50000, 1, 2, 1, 2],
            Star::SPECTRE_B => [10000, 30000, 0.1, 0.2, 0.15, 0.25],
            Star::SPECTRE_A => [7500, 10000, 0.1, 0.2, 0.15, 0.25],
            Star::SPECTRE_F => [6000, 7500, 0.5, 0.8, 0.8, 1.1],
            Star::SPECTRE_G => [5200, 6000, 0.3, 0.5, 0.5, 0.7],
            Star::SPECTRE_K => [3700, 5200, 0.2, 0.5, 0.3, 0.6],
            Star::SPECTRE_M => [2300, 3700, 0.05, 0.2, 0.05, 0.3],
        ],
        Star::TYPE_SUBGIANT => [
            Star::SPECTRE_O => [30000, 50000, 15, 30, 7, 30],
            Star::SPECTRE_B => [10000, 30000, 5, 17, 3.5, 10],
            Star::SPECTRE_A => [7500, 10000, 1.5, 2.5, 2, 4],
            Star::SPECTRE_F => [6000, 7500, 0.6, 1.7, 2, 4],
            Star::SPECTRE_G => [5200, 6000, 0.5, 1.4, 7, 4],
            Star::SPECTRE_K => [3700, 5200, 0.5, 1.3, 30, 7],
            Star::SPECTRE_M => [2300, 3700, 0.5, 1.2, 50, 30],
        ],
        Star::TYPE_GIANT => [
            Star::SPECTRE_O => [30000, 50000, 15, 50, 15, 50],
            Star::SPECTRE_B => [10000, 30000, 4, 20, 5, 15],
            Star::SPECTRE_A => [7500, 10000, 2, 4, 4, 6],
            Star::SPECTRE_F => [6000, 7500, 1, 2, 4, 6],
            Star::SPECTRE_G => [5200, 6000, 1.0, 1.7, 15, 6],
            Star::SPECTRE_K => [3700, 5200, 0.9, 1.5, 40, 15],
            Star::SPECTRE_M => [2300, 3700, 1.1, 1.4, 100, 40],
        ],
        Star::TYPE_BRIGHT_GIANT => [
            Star::SPECTRE_O => [30000, 50000, 10, 15, 50, 35],
            Star::SPECTRE_B => [10000, 30000, 5, 30, 35, 10],
            Star::SPECTRE_A => [7500, 10000, 4, 12, 60, 20],
            Star::SPECTRE_F => [6000, 7500, 2, 6, 80, 30],
            Star::SPECTRE_G => [5200, 6000, 3, 8, 120, 40],
            Star::SPECTRE_K => [3700, 5200, 3, 12, 200, 30],
            Star::SPECTRE_M => [2300, 3700, 2, 10, 500, 200],
        ],
        Star::TYPE_SUPERGIANT => [
            Star::SPECTRE_O => [30000, 50000, 50, 100, 35, 25],
            Star::SPECTRE_B => [10000, 30000, 16, 50, 60, 35],
            Star::SPECTRE_A => [7500, 10000, 12, 16, 80, 60],
            Star::SPECTRE_F => [6000, 7500, 10, 12, 120, 80],
            Star::SPECTRE_G => [5200, 6000, 10, 13, 200, 120],
            Star::SPECTRE_K => [3700, 5200, 12, 14, 500, 200],
            Star::SPECTRE_M => [2300, 3700, 12, 14, 1000, 500],
        ],
        Star::TYPE_HYPERGIANT => [
            Star::SPECTRE_O => [30000, 50000, 50, 200, 35, 500],
            Star::SPECTRE_B => [10000, 30000, 20, 100, 60, 700],
            Star::SPECTRE_A => [7500, 10000, 25, 50, 80, 1000],
            Star::SPECTRE_F => [6000, 7500, 20, 60, 120, 1500],
            Star::SPECTRE_G => [5200, 6000, 20, 50, 200, 2000],
            Star::SPECTRE_K => [3700, 5200, 15, 40, 500, 3000],
            Star::SPECTRE_M => [2300, 3700, 10, 30, 1000, 5000],
        ],
    ];

    public static function randomStarType(): int
    {
        $r = random_int(0, 1200);
        if ($r === 666) {
            return Star::TYPE_BLACK_HOLE;
        } elseif ($r === 777) {
            return Star::TYPE_NEUTRON_STAR;
        } elseif ($r < 5) {
            return Star::TYPE_HYPERGIANT;
        } elseif ($r > 1000) {
            return Star::TYPE_BROWN_DWARF;
        } elseif ($r < 800) {
            return Star::TYPE_DWARF;
        } elseif ($r < 900) {
            return Star::TYPE_WHITE_DWARF;
        } elseif ($r < 920) {
            return Star::TYPE_SUBDWARF;
        } elseif ($r < 960) {
            return Star::TYPE_SUBGIANT;
        } elseif ($r < 980) {
            return Star::TYPE_GIANT;
        } elseif ($r < 995) {
            return Star::TYPE_BRIGHT_GIANT;
        } else {
            return Star::TYPE_SUPERGIANT;
        }
    }

    public static function new(?int $preferredType = null): self
    {
        $generator = new self();
        if ($preferredType && random_int(0, 10) > 5) {
            $generator->type = $preferredType;
        } else {
            $generator->type = self::randomStarType();
        }
        return $generator;
    }

    public function withName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function withSpectre(int $spectre): self
    {
        $this->spectre = $spectre;
        return $this;
    }

    public function withRandomSpectre(): self
    {
        if ($this->type === Star::TYPE_BROWN_DWARF) {
            $this->spectre = ArrayHelper::randomValue(self::BROWN_DWARFS_SPECTRES);
            if ($this->spectre === Star::SPECTRE_M) {
                $this->spectreNumber = random_int(8, 9);
            }
        } elseif ($this->type === Star::TYPE_WHITE_DWARF) {
            $this->spectre = ArrayHelper::randomValue(self::WHITE_DWARF_SPECTRES);
        } elseif ($this->type === Star::TYPE_NEUTRON_STAR || $this->type === Star::TYPE_BLACK_HOLE) {
            $this->spectre = null;
        } else {
            $r = random_int(0, 1000);
            if ($r < 600) {
                $this->spectre = Star::SPECTRE_M;
            } elseif ($r < 700) {
                $this->spectre = Star::SPECTRE_K;
            } elseif ($r < 800) {
                $this->spectre = Star::SPECTRE_G;
            } elseif ($r < 900) {
                $this->spectre = Star::SPECTRE_F;
            } elseif ($r < 950) {
                $this->spectre = Star::SPECTRE_A;
            } elseif ($r < 990) {
                $this->spectre = Star::SPECTRE_B;
            } else {
                $this->spectre = Star::SPECTRE_O;
            }
            $this->spectreNumber = random_int(0, 9);
        }
        return $this->generateTemperatureMassRadius();
    }

    protected function generateTemperatureMassRadius(): self
    {
        [$minTemp, $maxTemp, $startMass, $endMass, $startRadius, $endRadius] = self::STAR_PARAMS_BY_SPECTRE[$this->type][$this->spectre];
        if ($this->type === Star::TYPE_BLACK_HOLE) {
            $this->temperature = 0;
        } elseif ($this->type === Star::TYPE_NEUTRON_STAR || $this->type === Star::TYPE_BROWN_DWARF || $this->type === Star::TYPE_WHITE_DWARF) {
            $this->temperature = random_int($minTemp, $maxTemp);
        } else {
            $delta = $maxTemp - $minTemp;
            $k = $delta > 0 ? (10 - $this->spectreNumber) / 10 : $this->spectreNumber / 10;
            $this->temperature = $delta*$k + $minTemp;
        }

        if ($this->spectreNumber === null) {
            $this->mass = random_int(min($startMass, $endMass)*10000, max($endMass, $startMass)*10000) / 10000;
            $this->radius = random_int(min($startRadius, $endRadius)*10000, max($endRadius, $startRadius)*10000) / 10000;
        } else {
            $delta = $endMass - $startMass;
            $k = $delta > 0 ? (10 - $this->spectreNumber) / 10 : $this->spectreNumber / 10;
            $this->mass = $startMass + $delta * $k;

            $delta = $endRadius - $startRadius;
            $k = $delta > 0 ? (10 - $this->spectreNumber) / 10 : $this->spectreNumber / 10;
            $this->radius = $startRadius + $delta * $k;
        }
        $this->luminosity = ($this->radius ** 2) * (($this->temperature / self::SOL_TEMPERATURE) ** 4);
        $this->luminosity += $this->luminosity * random_int(-12, 12) / 100;
        return $this;
    }

    public function generate(System $system): Star
    {
        $star = new Star([
            'systemId' => $system->id,
            'name' => $this->name,
            'mass' => $this->mass,
            'radius' => $this->radius,
            'luminosity' => $this->luminosity,
            'temperature' => $this->temperature,
            'type' => $this->type,
            'spectre' => $this->spectre,
            'spectreNumber' => $this->spectreNumber,
            'orbitAroundId' => $this->orbitAroundId,
            'orbitRadius' => $this->orbitRadius,
            'orbitEccentricity' => $this->orbitEccentricity,
        ]);
        return $star;
    }

}