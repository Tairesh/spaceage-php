<?php

namespace app\models\astro;

use app\models\base\ActiveRecord;

/**
 * This is the model class for table "satellites".
 *
 * @property int $id
 * @property int $systemId
 * @property string $name
 * @property float $mass
 * @property float $radius
 * @property float $gravity
 * @property float $temperatureEff
 * @property float|null $temperature
 * @property int $type
 * @property int|null $tectonicsType
 * @property string|null $atmosphere
 * @property string|null $hydrosphere
 * @property float $albedo
 * @property float|null $magneticField
 * @property int|null $orbitAroundId
 * @property float|null $orbitRadius In megametres
 * @property float|null $orbitEccentricity
 * @property float|null $day
 * @property float|null $month
 * @property int|null $tidalLock
 * @property float|null $axialTilt
 *
 * @property Planet $orbitAround
 * @property System $system
 */
class Moon extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'satellites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['systemId', 'name', 'mass', 'radius', 'gravity', 'temperatureEff', 'type', 'albedo'], 'required'],
            [['systemId', 'type', 'tectonicsType', 'orbitAroundId', 'tidalLock'], 'default', 'value' => null],
            [['systemId', 'type', 'tectonicsType', 'orbitAroundId', 'tidalLock'], 'integer'],
            [['mass', 'radius', 'gravity', 'temperatureEff', 'temperature', 'albedo', 'magneticField', 'orbitRadius', 'orbitEccentricity', 'day', 'month', 'axialTilt'], 'number'],
            [['atmosphere', 'hydrosphere'], 'safe'],
            [['name'], 'string', 'max' => 128],
            [['orbitAroundId'], 'exist', 'skipOnError' => true, 'targetClass' => Planet::class, 'targetAttribute' => ['orbitAroundId' => 'id']],
            [['systemId'], 'exist', 'skipOnError' => true, 'targetClass' => System::class, 'targetAttribute' => ['systemId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'systemId' => 'System ID',
            'name' => 'Name',
            'mass' => 'Mass',
            'radius' => 'Radius',
            'gravity' => 'Gravity',
            'temperatureEff' => 'Temperature Eff',
            'temperature' => 'Temperature',
            'type' => 'Type',
            'tectonicsType' => 'Tectonics Type',
            'atmosphere' => 'Atmosphere',
            'hydrosphere' => 'Hydrosphere',
            'albedo' => 'Albedo',
            'magneticField' => 'Magnetic Field',
            'orbitAroundId' => 'Orbit Around ID',
            'orbitRadius' => 'Orbit Radius',
            'orbitEccentricity' => 'Orbit Eccentricity',
            'day' => 'Day',
            'month' => 'Month',
            'tidalLock' => 'Tidal Lock',
            'axialTilt' => 'Axial Tilt',
        ];
    }

    /**
     * Gets query for [[OrbitAround]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrbitAround()
    {
        return $this->hasOne(Planet::class, ['id' => 'orbitAroundId']);
    }

    /**
     * Gets query for [[System]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSystem()
    {
        return $this->hasOne(System::class, ['id' => 'systemId']);
    }
}
