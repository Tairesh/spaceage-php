<?php

namespace app\models\auth;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Description of RegistrationForm
 *
 * @author ilya
 */
class RegistrationForm extends Model
{

    /**
     *
     * @var User
     */
    public ?User $identity = null;
    
    /**
     *
     * @var string
     */
    public string $email = '';

    /**
     *
     * @var string
     */
    public string $name = '';

    /**
     *
     * @var string
     */
    public string $password = '';

    /**
     *
     * @var string
     */
    public string $passwordConfirm = '';

    /**
     *
     * @var boolean
     */
    public bool $agreeTerms = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['email', 'name', 'password', 'passwordConfirm', 'agreeTerms'], 'required'],
            [['email', 'name'], 'string', 'max' => 256],
            [['password', 'passwordConfirm'], 'string'],
            [['email'], 'unique', 'targetClass' => User::class, 'message' => 'This email allready taken.'],
            [['email'], 'email'],
            [['passwordConfirm'], 'repeatedPassword'],
            [['agreeTerms'], 'isTrue'],
        ];
    }

    public function repeatedPassword($attribute, $params = [])
    {
        if ($this->$attribute !== $this->password) {
            $this->addError($attribute, Yii::t('app', 'Passwords not match.', [
                        $this->getAttributeLabel($attribute),
                        $this->getAttributeLabel('password'),
            ]));
            return false;
        }
        return true;
    }

    public function isTrue($attribute, $params = [])
    {
        if (!$this->$attribute) {
            $this->addError($attribute, Yii::t('app', 'You really should agree the terms.'));
            return false;
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'agreeTerms' => 'I agree terms',
            'name' => 'Displayed name',
        ];
    }

    public function register()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $identity = new User([
            'email' => $this->email,
            'name' => $this->name,
        ]);
        $identity->setPassword($this->password);
        $identity->generateAuthKey();
        $identity->save();
        $this->identity = $identity;
        Yii::$app->user->login($identity);
        return true;
    }

}
