<?php


namespace app\models\auth;

use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class AvatarForm extends Model
{

    /**
     *
     * @var UploadedFile|null|string
     */
    public $avatarFile = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avatarFile'], 'required'],
            [['avatarFile'], 'file'],
            [['avatarFile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'avatarFile' => 'New avatar',
        ];
    }

    public function proceed(): bool
    {
        $this->avatarFile = UploadedFile::getInstance($this, 'avatarFile');
        if (!$this->avatarFile) {
            $this->addError('avatarFile', 'File not uploaded');
            return false;
        }
        if ($this->avatarFile->error) {
            $this->addError('avatarFile', 'Error #'.$this->avatarFile->error);
            return false;
        }

        $user = Yii::$app->user->identity;
        $path = Yii::getAlias("@webroot/upload/avatars/{$user->getId()}/");
        if (!is_dir($path) && !mkdir($path) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        Image::thumbnail($this->avatarFile->tempName , 50, 50)->save($path.'50.jpg', ['quality' => 80]);
        Image::resize($this->avatarFile->tempName, 300, null, true)->save($path.'300.jpg', ['quality' => 80]);

        $user->avatarBig = '/upload/avatars/'.$user->getId().'/300.jpg';
        $user->avatar = '/upload/avatars/'.$user->getId().'/50.jpg';
        return $user->save();
    }

}