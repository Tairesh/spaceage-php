<?php


namespace app\helpers;


class ArrayHelper extends \yii\helpers\ArrayHelper
{

    /**
     * @param array $ar
     * @return mixed
     */
    public static function randomValue(array $ar)
    {
        return $ar[array_rand($ar)];
    }

}