<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200420_053050_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(128)->unique()->notNull(),
            'password' => $this->string(128)->defaultValue(null),
            'authKey' => $this->string(64)->unique()->notNull(),
            'role' => $this->smallInteger(2)->unsigned()->notNull()->defaultValue(1),
            'status' => $this->smallInteger(2)->unsigned()->notNull()->defaultValue(0),
            'name' => $this->string(128)->notNull(),
            'avatar' => $this->text()->null(),
            'avatarBig' => $this->text()->null(),
            'dateCreated' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->createIndex('usersEmail', '{{%users}}', ['email'], true);
        $this->createIndex('usersAuthKey', '{{%users}}', ['authKey'], true);
        $this->createIndex('usersStatus', '{{%users}}', ['status']);
        $this->createIndex('usersRole', '{{%users}}', ['role']);
        $this->createIndex('usersDateCreated', '{{%users}}', ['dateCreated']);
        $this->createIndex('usersName', '{{%users}}', ['name']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('usersEmail', '{{%users}}');
        $this->dropIndex('usersAuthKey', '{{%users}}');
        $this->dropIndex('usersStatus', '{{%users}}');
        $this->dropIndex('usersRole', '{{%users}}');
        $this->dropIndex('usersDateCreated', '{{%users}}');
        $this->dropIndex('usersName', '{{%users}}');
        $this->dropTable('{{%users}}');
    }
}
