<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%moons}}`.
 */
class m200420_124510_create_moons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%moons}}', [
            'id' => $this->primaryKey(),
            'systemId' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(128)->notNull(),
            'mass' => $this->float()->notNull(),
            'radius' => $this->float()->notNull(),
            'gravity' => $this->float()->notNull(),
            'temperatureEff' => $this->float()->notNull(),
            'temperature' => $this->float(),
            'type' => $this->smallInteger(3)->unsigned()->notNull(),
            'tectonicsType' => $this->smallInteger(2)->unsigned()->null(),
            'atmosphere' => $this->json()->null(),
            'hydrosphere' => $this->json()->null(),
            'albedo' => $this->float()->unsigned()->notNull(),
            'magneticField' => $this->float()->unsigned()->null(),
            'orbitAroundId' => $this->integer()->unsigned()->null(),
            'orbitRadius' => $this->float()->null()->comment('In megametres'),
            'orbitEccentricity' => $this->float()->unsigned()->null(),
            'day' => $this->float()->null(),
            'month' => $this->float()->null(),
            'tidalLock' => $this->smallInteger(2)->unsigned()->null(),
            'axialTilt' => $this->float()->null(),
        ]);
        $this->addForeignKey('moonsSystemForeign', '{{%moons}}', ['systemId'], '{{%systems}}', ['id']);
        $this->addForeignKey('moonsOrbitAround', '{{%moons}}', ['orbitAroundId'], '{{%planets}}', ['id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('moonsSystemForeign', '{{%moons}}');
        $this->dropForeignKey('moonsOrbitAround', '{{%moons}}');
        $this->dropTable('{{%moons}}');
    }
}
