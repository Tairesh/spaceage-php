<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%systems}}`.
 */
class m200420_112849_create_systems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%systems}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'x' => $this->integer()->notNull(),
            'y' => $this->integer()->notNull(),
        ]);

        $this->createIndex('systemsXY', '{{%systems}}', ['x', 'y'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('systemsXY', '{{%systems}}');
        $this->dropTable('{{%systems}}');
    }
}
