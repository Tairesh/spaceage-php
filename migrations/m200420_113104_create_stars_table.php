<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stars}}`.
 */
class m200420_113104_create_stars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stars}}', [
            'id' => $this->primaryKey(),
            'systemId' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(128)->notNull(),
            'mass' => $this->float()->notNull(),
            'radius' => $this->float()->notNull(),
            'luminosity' => $this->float()->notNull(),
            'temperature' => $this->float()->notNull(),
            'type' => $this->smallInteger(3)->notNull(),
            'spectre' => $this->smallInteger(3)->null(),
            'spectreNumber' => $this->smallInteger(1)->null(),
            'orbitAroundId' => $this->integer()->unsigned()->null(),
            'orbitRadius' => $this->float()->null()->comment('In megametres'),
            'orbitEccentricity' => $this->float()->unsigned()->null(),
        ]);
        $this->addForeignKey('StarsSystemForeign', '{{%stars}}', ['systemId'], '{{%systems}}', ['id']);
        $this->addForeignKey('StarOrbitAround', '{{%stars}}', ['orbitAroundId'], '{{%stars}}', ['id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('StarsSystemForeign', '{{%stars}}');
        $this->dropForeignKey('StarOrbitAround', '{{%stars}}');
        $this->dropTable('{{%stars}}');
    }
}
