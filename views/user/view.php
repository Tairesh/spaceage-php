<?php

use app\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this \yii\web\View */
/* @var $model \app\models\User */
/* @var $viewer \app\models\User */
/* @var $isOwner boolean */
/* @var $avatarFormModel \app\models\auth\AvatarForm|null */

$this->title = Yii::$app->name . ' / User: ' . $model->name;

?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class=" box" >
                <div class="box-body">
                    <?=Html::img($model->avatarBig ?? 'http://placekitten.com/g/300/300', ['class' => 'img-polaroid', 'style' => 'width: 100%'])?>
                </div>
                <?php if ($isOwner): ?>
                    <div class="box-footer">
                    <?php $form = ActiveForm::begin([
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                        ]) ?>
                        <?= $form->field($avatarFormModel, 'avatarFile')->widget(FileInput::class, [
                            'options'=>[
                                'multiple' => false,
                                'accept' => 'image/*',
                            ],
                            'pluginOptions' => [
                                'previewFileType' => 'image',
                                'showRemove' => false,
                                'showUpload' => false,
                                'showCaption' => false,
                                'browseClass' => 'btn btn-primary btn-flat',
                                'browseIcon' => '<i class="fa fa-camera"></i> ',
                                'browseLabel' =>  'Select image',
                            ],
                        ]) ?>
                        <?= Html::submitButton('Upload avatar', ['class' => 'btn btn-flat btn-success']) ?>
                    <?php ActiveForm::end() ?>
                    </div>
                    <!-- /.box-footer-->
                <?php endif ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-header">
                    <h1><?= Html::encode($model->name) ?> <?php if ($isOwner): ?><small>(it's you)</small><?php endif ?></h1>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <?php if ($isOwner): ?>
                            <tr>
                                <td>Email</td>
                                <td><?= $model->email ?></td>
                            </tr>
                        <?php endif ?>
                            <tr>
                                <td>Registration date</td>
                                <td><?= Html::timeAutoFormat($model->dateCreated) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                <?php if ($isOwner): ?>
                    <div class="btn-toolbar">
                        <div class="btn-group">
                            
                        </div>
                    </div>
                <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>
