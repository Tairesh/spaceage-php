<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\helpers\DateHelper;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$time = time();

$dateHuman = DateHelper::getTimeAgo($time);
$dateOnServer = DateHelper::dateInClientTimezone('d.m.Y H:i', $time);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#292929">
    <meta name="author" content="Ilya Agafonov">
    <link rel="shortcut icon" href="/favicon.ico">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => Yii::$app->user->isGuest ? [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'Login', 'url' => ['/auth/login']],
                ] : [
                    '<li class="dropdown">'
                    . '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'
                    . (Yii::$app->user->identity->avatar
                        ? Html::img(Yii::$app->user->identity->avatar, ['style' => 'height: 16px; vertical-align: top;'])
                        : '<span class="glyphicon glyphicon-user"></span>')
                    . ' ' . Yii::$app->user->identity->name . ' <span class="caret"></span></a>'
                    . '<ul class="dropdown-menu">'
                    . '<li>'
                    . Html::a('<span class="glyphicon glyphicon-user"></span> Profile', ['/user/view', 'id' => Yii::$app->user->getId()])
                    . '</li>'
                    . '<li>'
                    . Html::a('<span class="glyphicon glyphicon-log-out"></span> Logout', ["auth/logout"], [
                                    'data' => [
                                        'confirm' => 'Are you sure you want to sign out?',
                                        'method' => 'post',
                                    ]
                                ])
                    . '</li>'
                    . '</ul>'
                    . '</li>',

                ],
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => $this->params['breadcrumbs'] ?? [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">Page generated <span class="date" data-timestamp="<?=$time?>"><?=$dateOnServer?></span> (<?=$dateHuman?>)</p>

            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
